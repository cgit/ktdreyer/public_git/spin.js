%global commit 63b791dd1e7139694f1caf02726dab1ac33e0177
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%global         checkout 20130125git%{shortcommit}

Name:           spin.js
Version:        1.2.7
Release:        1.%{checkout}%{?dist}
Summary:        A spinning activity indicator
Group:          Applications/Internet
License:        MIT
URL:            http://fgnass.github.com/spin.js/
Source0:        https://github.com/fgnass/spin.js/archive/%{commit}/%{name}-%{version}-%{shortcommit}.tar.gz
Source1:        %{name}.conf
BuildArch:      noarch

#BuildRequires:  
Requires:       httpd

%description
An animated CSS3 loading spinner with VML fallback for IE.


%prep
%setup -q -n %{name}-%{commit}

%build

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p dist/spin.js  $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p dist/spin.min.js $RPM_BUILD_ROOT%{_datadir}/%{name}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d
install -p -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d

%files
%doc assets index.html LICENSE.txt README.md
%{_datadir}/%{name}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/%{name}.conf

%changelog
* Fri Jan 24 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.1.0-1.20130124gitdbe241f
- Initial packaging
